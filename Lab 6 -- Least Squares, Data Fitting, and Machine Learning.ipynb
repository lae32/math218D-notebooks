{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Least Squares\n",
    "\n",
    "## Pre-lab Reading: Bias-Variance (do this *before* the lab!)\n",
    "\n",
    "Read about bias-variance [here](http://scott.fortmann-roe.com/docs/BiasVariance.html). Make sure you can answer the following questions:\n",
    "* Why do we want a predictor not to be *biased*?\n",
    "* Why do we want a predictor not to have large *variance*?\n",
    "* Why do these two often work in opposition to one another?\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "## Least Squares as an Overdetermined System\n",
    "\n",
    "**Question 1** Consider the three points $(1,5)$, $(2,12)$, and $(4,26)$. \n",
    "1. Using high-school algebra, find an equation for line passing through these three points.<br><br>\n",
    "1. Write a matrix equation $Ax=b$ whose solution gives the line above. (Hint: If $y=mx+c$, write three equations that must be satisfied by $m$ and $c$, then translate your system into matrix form.)<br><br>\n",
    "1. Since there is a solution to your system, the vector $b$ must be in what fundamental space of the matrix $A$?\n",
    "\n",
    "**Question 2** Now consider the points $(1,4.7)$, $(2,11.6)$, and $(4,26.4)$. Is there a line that passes through these three points? Justify your answer using the language of linear algebra (matrices and fundamental spaces).\n",
    "\n",
    "As you may have noticed, these points are very close to the ones in the previous question. That is, the line you found in Question 1 is a *pretty good* fit for the points in Question 2. It makes sense to ask 'just how good is it?' and 'is there a better line'?\n",
    "\n",
    "Regardless of these questions, we can see both systems above as *overdetermined*. That is, there are more equations than variables. Overdetermined systems can either have a unique solution or no solution at all. We will see today that many problems can be cast in terms of using linear algebra techniques to find a 'best' model in the case that there is no solution.\n",
    "\n",
    "\n",
    "## Fitting a Polynomial to Data\n",
    "\n",
    "\n",
    "**Question 3** Look back at the last part of Lab 3, where we used LU decomposition to fit an $n-1$ degree polynomial to $n$ data points.\n",
    "1. Use the techniques from Lab 3 to find a quadratic polynomial that fits the points from Question 2.<br><br>\n",
    "1. Plot your points, the line you found in Question 1, and the polynomial from the previous part of this question on the same axes. <br><br>\n",
    "1. Suppose that these points were the result of an experiment that produced the $y$-values from $x$-values. Suppose we wanted to use a model to find what the $y$ value corresponding to, say, $x=10$ should be. Which equation would you feel more comfortable using, the line or the polynomial? After you've done the reading in the pre-lab, explain your answer in terms of bias and variance. You may want to change your points slightly, recompute the quadratic, and look at how your predictions for $x=10$ vary.\n",
    "\n",
    "\n",
    "**Note:** To plot the functions, say, $x^2$ and $x^3$ in blue and red respectively, with domain $-25<x<25$, and the points $(1,4)$, $(5,0)$, and $(6,2)$, you can use the following code:\n",
    "\n",
    "```python\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "x = np.linspace(-25,25,100) # 100 linearly spaced numbers - read up on the np.linspace() command!\n",
    "y1 = x**2\n",
    "y2 = x**3\n",
    "plt.plot(x,y1,'b',x,y2,'r')\n",
    "plt.scatter([1,5,6],[4,0,2])\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Least Squares\n",
    "\n",
    "\n",
    "### The 2-D Case\n",
    "\n",
    "As you talked about in class, the best fit line for a set of data is given by the line that minimizes the sum of the squares of the distances of the data points from the line.\n",
    "\n",
    "**Question 4** \n",
    "1. Suppose that the points $(x_1,y_1)$, $(x_2,y_2)$, and $(x_3,y_3)$ are collinear (that is, they fall on the same line). Write down a matrix equation for $m$ and $c$ (Hint: this is similar to Question 1). Explain in terms of linear combinations and fundamental spaces why your equation has a solution.<br><br>\n",
    "1. Suppose that the points $(x_1,y_1)$, $(x_2,y_2)$, and $(x_3,y_3)$ are *not* collinear. Write down a matrix equation for $m$ and $c$. Explain why your equation *does not* have a solution. Again, write your answer in terms of linear combinations and fundamental spaces.<br><br>\n",
    "1. If the three points are not collinear, then the vector on the right side of your equation is not in in the $\\underline{~~~~~~~~~~~~~~~~~~~~~~~~~~~}$ space of the matrix on the left. (Fill in the blank.)\n",
    "\n",
    "As we learned in class, when we cannot find a solution $x$ to the equation $Ax=b$, we can project $b$ onto the closest point $p$ in the $\\underline{~~~~~~~~~~~~~~~~~~~~~~~~~~~}$ space of $A$ and solve $A\\hat{x}=p$ instead. To do this, we solve the *normal equation*:\n",
    "\n",
    "$$\\large{A^TA\\hat{x}=A^Tb}$$\n",
    "\n",
    "**Question 5** Use your LU decomposition code from prior labs and the normal equation to find the best fit line to the set of points from Question 2. Plot the points and your line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 6** (From Larsen, R.J., and Marx, M.L. (1986). An Introduction to Mathematical Statistics and its Applications, 2nd Edition. Case study 1.2.4., also from the first 105L Lab):\n",
    "\n",
    "> Since World War II, plutonium for use in atomic weapons has been produced at an Atomic Energy Commission facility in Hanford, Washington.  One of the major safety problems encountered there has been the storage of radioactive wastes.  Over the years, significant quantities of these substances, including strontium 90 and cesium 137---have leaked from their open-pit storage areas into the nearby Columbia River, which flows along the Washington-Oregon border, and eventually empties into the Pacific Ocean.\n",
    "\n",
    "To measure the extent to which exposure to the radiation affected cancer mortality rates during 1959-1964, researchers calculated an *index of exposure*.  The index takes into consideration the county's distance down the river from the plant and the distance from the river.  More precisely, the index is formulated on the assumption that county or city exposure is directly proportional to river frontage and inversely proportional both to the distance from the Hanford site and the square of the county's (or city's) average depth away from the river (David A. Smith and L.C. Moore, Calculus: Modeling and Application, 1996).  The following table gives the index of exposure and cancer mortalities (the average number of cancer deaths per 20,000 residents per year during the period 1959-1964).\n",
    "\n",
    "\n",
    "County | Exposure | Mortality |\n",
    "--- |:---:| ---:| \n",
    "Umatilla | 2.49 | 147.1 |\n",
    "Morrow | 2.57 | 130.1 |\n",
    "Gilliam | 3.41 | 129.9 |\n",
    "Sherman | 1.25 | 113.5 |\n",
    "Wasco | 1.62 | 137.5 |\n",
    "Hood River | 3.83 | 162.3 |\n",
    "Portland | 11.64 | 207.5 |\n",
    "Columbia | 6.41 | 177.9 |\n",
    "Clatsop | 8.34 | 210.3 |\n",
    "\n",
    "Compute a best fit line for this data and use to predict the mortality for a new development along the river whose index of exposure is calculated to be 10.\n",
    "\n",
    "**Note:** If you took 105L, compare your answer to the best fit line presented in the Cancer Mortality lab in that class.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting Polynomials to Data\n",
    "\n",
    "We saw above that fitting a line to data involved finding the slope and $y$-intercept of the line $y=mx+c$. Similarly, a quadratic function is given by $f(x)=ax^2+bx+c$.\n",
    "\n",
    "**Question 7** Consider the points $(1,2)$, $(2,4)$, and $(5,-1)$.\n",
    "* If a quadratic function $f(x)=ax^2+bx+c$ passes through these points, write down three equations satisfied by $a$, $b$, and $c$.<br><br>\n",
    "* Rewrite your equations as a matrix equation.\n",
    "\n",
    "**Note:** The question above is very similar to the work we did at the end of the LU decomposition lab and its associated homework.\n",
    "\n",
    "**Question 8** Consider the four points $(-1,4)$, $(2,-10)$, $(0,-2)$, and $(4,5)$. By using the ideas from question 7 and the previous part, find a *best fit parabola* for these points. Plot your answer and the points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Affine Functions in Higher Dimensions\n",
    "\n",
    "In two dimensions, any line can written as $y=mx+c$. We can rearrange this as $y-mx-c=0$. In general, an *affine function* $f:\\mathbb{R}^2\\to\\mathbb{R}$ can be written as \n",
    "\n",
    "$$f(x,y)=ax+by+c.$$ \n",
    "\n",
    "\n",
    "**Note:** This is slightly different from a *linear* function. Linear functions always map the zero vector in the domain to the zero vector in the range. In this case, what condition would you have to impose on the above function to make it linear?\n",
    "\n",
    "\n",
    "An affine function in a higher dimension $n$, $f:\\mathbb{R}^n\\to\\mathbb{R}$ can be written as\n",
    "\n",
    "$$f(x_1,x_2,\\ldots,x_n)=a_0 + a_1x_1 + a_2x_2 + \\ldots + a_nx_n$$\n",
    "\n",
    "**Note:** What condition on this would make the function linear?\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Machine Learning: Binary Classifiers\n",
    "\n",
    "A basic problem in machine learning is: 'Given a set of data, decide which part of it belongs in set A, and which does not. For example, given a set of images, we might ask 'Which of these contain a cat?'. The problem is building a *binary classifier*: \n",
    "\n",
    "* A *classifier* divides a data set into different parts;<br><br>\n",
    "* A classifier is *binary* if it divides the data set into precisely two parts.\n",
    "\n",
    "For example, a classifier that divides all images into 'those containing a cat' and 'those not containing a cat' is a binary classifier.\n",
    "\n",
    "The goal today is to build a binary classifier that will distinguish handwritten zeros from other handwritten digits. We will get there in a little while, but we'll start with two simpler examples.\n",
    "\n",
    "### Classifying Points in the Plane\n",
    "\n",
    "We'll start by generating random points in two clusters that are quite far apart in order to understand what our classifiers are going to look like, and how we compute them. We'll then look at the same idea when the two clusters overlap, and the answer isn't so straightforward.\n",
    "\n",
    "#### Two Distinct Clusters\n",
    "\n",
    "We will generate 200 random points in the $x-y$ plane. 100 of our points will be near $(2,3)$, and the rest near $(-4,-4)$. The command\n",
    "```python\n",
    "np.random.rand(n)\n",
    "```\n",
    "generates an vector of $n$ random numbers between $0$ and $1$, drawn from a uniform distribution (that is, any number in the range $[0,1]$ is equally likely to occur).\n",
    "\n",
    "**Question 9** Write code to generate two $100\\times2$ uniformlly distributed random arrays, where each row represents a point in the $x-y$ plane. The first array should contain points in a rectangle with opposite corners $(1.5,2)$ and $(2.5,4)$; The second array should contain points in a rectangle with opposite corners $(-5,-5)$ and $(-3,3)$. Plot your points, one set in red and the other in blue.\n",
    "\n",
    "**Note:** It is good NumPy practice to first create arrays of the right shape (using, e.g., `np.zeros((m,n))`, then fill them.) \n",
    "\n",
    "If the *x*-coordinates of your points are in the array *X1* and the *y*-coordinates in the array *Y1*, the following code plots the points in blue. Changing the **b** to **r** plots in red:\n",
    "```python\n",
    "plt.scatter(X1,Y1,c='b')\n",
    "```\n",
    "\n",
    "Lastly, the command\n",
    "```python\n",
    "plt.gca().set_aspect('equal') # Note: gca is 'get current axes'\n",
    "```\n",
    "will ensure your scales are the same. This may or may not be a good thing! It's up to you to decide when to use equal scales."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 10** By looking at your plot above, write down a yes/no question that, given a point, will test whether it belongs in the red cluster or the blue.\n",
    "\n",
    "#### Less Distinct Clusters\n",
    "\n",
    "**Question 11** Modify your above code so that the first cluster is in the square cornered by $(5,5)$ and $(10,10)$, and the second cluster is in a rectangle cornered by $(8,4)$ and $(12,8)$. Looking at your plot, can you come up with a yes/no question similar to the one above that will test which cluster a given point belongs in? Can you come up with a yes/no question that decides which cluster a given point *probably* belongs in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Our Classifiers\n",
    "\n",
    "Ideally, we'd have a function takes one value, say, 1, on points from one cluster, and another value, say, -1, on points in the other cluster. If we had such a function, we'd be able to plug the coordinates of a point whose cluster is unknown into the function and read off the cluster from the result. Such a function rarely exists, even in a simple case like our second pair of clusters above, and certainly not in the case of handwritten digits.\n",
    "\n",
    "Instead, our strategy will be to to use the idea of least squares to find a affine function that best approximates our desired function. We'll start with our clearly separated clusters from the first example.\n",
    "\n",
    "Our ideal function would take the first 100 points and map them to 1, and the second 100 points to -1. That is, if $(x_1,y_1)$ is a point in the first cluster, then we'd like to find $a$, $b$, and $c$ such that \n",
    "\n",
    "$$ax_1+by_1+c=1,$$ \n",
    "\n",
    "and similarly for the second cluster (where a point should map to $-1$). Why doesn't such a function exist?\n",
    "\n",
    "**Question 12** Create a $200\\times 3$ array $A$ whose first two columns are the points in the clusters in Question 9 and whose last column is all ones. Write a matrix equation $Ax=v$ involving the coefficients of the ideal function described above (You may want to refer to Questions 1-4 above). Specifically, what are the vectors *x* and *v*?\n",
    "\n",
    "**Question 13** Create the vector *v*, then use the ideas above (and your LU code) to find the affine function that minimizes the sum of least squares from your ideal vector. Rewrite your function in slope-intercept form (Hint: Rearrange the equation $ax+by+c=0$). Lastly, plot your clusters and your function on the same set of axes. You should get quite a pretty picture!\n",
    "\n",
    "**Note:** We pick zero for the right hand side of our plot because we would ideally want points likely to belong to one cluster to give us $1$, and points likely to belong to the other cluster to give us $-1$ when we plug them into $ax+by+c$. Zero, being half way in between, gives us (maybe...) the best separator. Suppose you were tasked with using your classifier to predict which cluster a *new point* $(x_0, y_0)$ would most likely belong to, if $ax_0+by_0+c$ is positive, the classifier would predict it belongs to the first cluster. If it is negative, the classifier predicts it would belong to the second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 14** Repeat Question 13 for your second set of two clusters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 15** Suppose that you were tasked with using your classifier to decide which cluster a given point $(x_1,y_1)$ belongs to. Clearly, plugging it into your affine function won't give $1$ or $-1$ in general. How would you use the result to decide which cluster your point is likely to be in? Feel free to experiment with a few points, of course!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next lab, we will apply these ideas to a large dataset and see if we can get a computer to recognize handwriting."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
